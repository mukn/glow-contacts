-- Test data for contacts.
PRAGMA foreign_keys=ON;

INSERT INTO network (name, description, uri, native_token, exchange_token) VALUES
  ('ced', 'Cardano EVM Devnet', 'https://rpc-evm.portal.dev.cardano.org/', 'CED', 'ADA'),
  ('etc', 'Ethereum Classic Mainnet', 'https://ethereumclassic.network', 'ETC', NULL),
  ('eth', 'Ethereum Mainnet', 'https://mainnet.infura.io/v3/${INFURA_API_KEY}', 'ETH', NULL),
  ('kot', 'Ethereum Classic Testnet Kotti', 'https://www.ethercluster.com/kotti', 'KOT', 'ETC'),
  ('kov', 'Ethereum Testnet Kovan', 'https://kovan.poa.network', 'KOV', 'ETC'),
  ('ogor', 'Optimistic Ethereum Testnet Goerli', 'https://www.ethercluster.com/goerli', 'GOR', 'ETC'),
  ('pet', 'Private Ethereum Testnet', 'http://localhost:8545', 'PET', 'ETH'),
  ('rin', 'Ethereum Testnet Rinkeby', 'https://rinkeby.infura.io/v3/${INFURA_API_KEY}', 'ETH', NULL),
  ('rop', 'Ethereum Testnet Ropsten', 'https://ropsten.infura.io/v3/${INFURA_API_KEY}', 'ETH', NULL);
