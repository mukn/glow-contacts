#!/bin/sh -e

dbfile=${1:-"$HOME/.config/glow/db/contacts.db"}
schemadir=${2:-$(dirname $0)}

# A successful load should not produce any warnings or output.
for sql in "$schemadir/contacts.sql" "$schemadir/initial-contacts.sql"; do
    sqlite3 "$dbfile" < "$sql"
done
