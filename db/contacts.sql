-- Contacts schema.

-- Drop tables in reverse order.
DROP TABLE IF EXISTS identity;
DROP TABLE IF EXISTS network;
DROP TABLE IF EXISTS contact;
DROP TABLE IF EXISTS txnlog;
DROP TABLE IF EXISTS editlog;

-- Missing tags.
CREATE TABLE contact (
  cid INTEGER PRIMARY KEY,
  name TEXT
);
CREATE UNIQUE INDEX contact_name ON contact(name);

CREATE TABLE network (
  name TEXT PRIMARY KEY NOT NULL,
  description TEXT,
  uri TEXT,
  native_token TEXT NOT NULL,
  exchange_token TEXT
);

CREATE TABLE identity (
  cid INTEGER REFERENCES contact ON DELETE CASCADE,
  network TEXT NOT NULL REFERENCES network,
  address TEXT NOT NULL,
  nickname TEXT,
  public_key TEXT,
  secret_key_path TEXT,
  CHECK (length(address) > 0),
  UNIQUE (cid, network, address)
);
CREATE UNIQUE INDEX identity_nickname ON identity(nickname);

CREATE TABLE txnlog (
  txid INTEGER PRIMARY KEY,
  timestamp TEXT NOT NULL DEFAULT CURRENT_TIMESTAMP,
  command TEXT NOT NULL,
  output TEXT NOT NULL DEFAULT '',
  status INTEGER
);

CREATE TABLE editlog (
  edid INTEGER PRIMARY KEY,
  timestamp TEXT NOT NULL DEFAULT CURRENT_TIMESTAMP,
  command TEXT NOT NULL,
  arguments TEXT NOT NULL DEFAULT '[]'
);

-- Missing assets/resources.
