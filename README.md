# Glow Contacts

[MuKn Glow](https://glow-lang.org/) is a language to write safe
Decentralized Applications (DApps) interacting with cryptocurrency
blockchains.

This library, `glow-contacts`, implements the storage and API
for **contacts** and **identities** in Glow. It manages names,
addresses, keys, etc. associated with the people or services
that a DApp written in Glow interacts with.

## Using

You can install `glow-contacts` using the Gerbil package manager:

```
gxpkg install gitlab.com/mukn/glow-contacts
```

Alternatively, you can clone the repo and build it yourself:
```
gxpkg install github.com/drewc/ftw
gxpkg install github.com/fare/gerbil-utils

git clone https://gitlab.com/mukn/glow-contacts.git
cd glow-contacts
gxpkg link gitlab.com/mukn/glow-contacts $PWD

./build.ss
```

Once it's installed, you can initialize the database with:
```
./db/makedb.sh
```
This will create an SQLite 3 database file in (by default)
`$HOME/.config/glow/db/contacts.db` (you can override that path
by supplying it as an argument to the `makedb.sh` script).

Then you can run the server:
```
./main.ss run-server
Glow contacts server running at 127.0.0.1:6742
```
By default the server runs in the foreground, and you can stop
it with `Ctrl-C` or any other kill signal.

In another terminal, you should now be able to make HTTP
requests to the server:

- **Add contact**

  ``` sh
  $ curl -d '{"name":"alice","identities":[{"network":"pet","address":"0x1234"}]}' \
    -X POST http://localhost:6742/contacts/contact
  Created contact 1
  ```

- **Get contact by cid**

  ```sh
  $ curl -sS 'http://localhost:6742/contacts/contact/1' | jq
  {
    "cid": 1,
    "identities": [
      {
        "address": "0x1234",
        "network": "pet"
      }
    ],
    "name": "alice"
  }
  ```

- **Get all contacts**

  ``` sh
  $ curl -sS 'http://localhost:6742/contacts/contacts' | jq
  [
    {
      "address": "0x1234",
      "name": "Alice",
      "network": "etc"
    },
    {
      "address": "0x1234",
      "name": "Alice",
      "network": "pet"
    },
    {
      "address": "0x5678",
      "name": "Bob",
      "network": "pet"
    },
    {
      "address": "0x9876",
      "name": "Carol",
      "network": "pet"
    },
    {
      "address": "0x6666",
      "name": "Mallory",
      "network": "pet"
    }
  ]
  ```

### Copyright and License

Copyright 2021 Mutual Knowledge Systems, Inc. All rights reserved.
Glow is distributed under the Apache License, version 2.0. See the
file [LICENSE](LICENSE).
